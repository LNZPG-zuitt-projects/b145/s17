console.log('hello from JS')

// [SECTION] CONTROL STRUC

// IF-ELSE STATEMENT
// Sample 1
let numA  = -1; 
// IF statemnt (branch)
	// true condition
if (numA <= 0) {
	// truth branch
	// if condition is met
	console.log('The condition was MET!');

}

// Sample 2
let name = 'Lorenzo';

if (name === 'Lorenzo') {
	console.log('User can proceed!');
}

// ELSE Statement
	// if first condition was not met.


// prompt box - displays prompt box which user can use for an input
//syntax: prompt(text/message[REQUIRED], placeholder/default text[OPTIONAL]); 
 //prompt("Please enter your first name:" , "Neil");


// Create function is that allows us to check if the user is old enough to drink,
let age = 21;
// alert() =>  displays message box to the user which would require theit attention
// if (age>=18) {
// 	// 'truthy' branch
// 	console.log("You're old enough to drink");
// } else {
// 	// 'falsy' branch
// 	alert("Come back another day! Gatas ka na lang muna buy");
// }

// create a control structure that will ask for the number of drinks that you will order
// ask the user how many drinks he wants
//let order = prompt('How many orders of drink do you like?');


// convert the string data type into a number
// parseInt() => will allow us to convert strings into integers
// order = parseInt(order);

// create a logic taht will make sure that the user's input is greater than 0
// if (order > 0) {
// 	console.log(typeof order);
// 	let cocktail = "🍸";
// 	alert(cocktail.repeat(order));
// } else {
// 	alert('The number should be above 0')
// }

// mini task: vaccine checker
function vaccineChecker() {
	// ask information from user
	let vax = prompt('Please input your vaccine');
	// pfizer, PFIZER
	// we need to process the inout of the user so that whatever inut he will enter we can control the uniformity of character casing
	// toLowerCase() -> will allow us to convert a string into all lowercase characters. (syntax: string.toLowerCasee())
	vax = vax.toLowerCase();
	// create a logic that will allow us to check the values inserted by the user to match the given parameters.......

	if (vax === 'pfizer' || vax === 'moderna' || vax === 'astrazeneca' || vax === 'janssen') {
		// display response to client
		alert(vax +' is allowed to travel');
	} else {
		alert('sorry not permitted');
	}
}

//onclick =>is an example of a JS Event. this "event" in JS occurs when the user clicks on an element/component to trigger a certain procedure. 

//syntax: <element/component onclick="myScript/Function" >


// Typhoon Checker
function determineTyphoonIntensity()
{
	// going to need an input from the user
	// needs number data type so we  can properly compare the values
	let windspeed = parseInt(prompt('Wind speed:'));
	console.log(typeof windspeed);

// note "else if" is 2 words
if (windspeed <= 29) {
    alert('Not a Typhoon Yet!'); 
 	 } else if (windspeed <= 65) {
    //this will run if the 2nd statement was met.
    alert('Tropical Depression Detected');
   } else if (windspeed <=88){
   	alert('Tropical Storm Detected')
   } else if(windspeed <=117){
    alert('Severe Tropical Storm')
   } else {
    alert('Typhoon Detected'); 
   }
}



// CONDITIONAL TERNARY OPERATOR

// still follows same syntax as if-else
// (truth,falsy)
// this is the only JS operator that takes 3 operands

// ? question mark  ==> describe the condition
// : colon

// syntax: condition ? "truthy" : "falsy"

function ageChecker ()
{
	let age = parseInt(prompt('How old are you?'));
	// simplification of code below with ternary operator
	return  (age >= 18) ? alert('Old enough to vote') :  alert('Not yet old enough');

	// if (age >= 18) {
	// 	// truthy
	// 	alert('Old enough to vote');
	// } else {
	// 	// falsy
	// 	alert('Not old enough');
	// }
}



function determineComputerOwner(){
	let unit = prompt('What is the unit no.?');

	let manggagamit;
	// unit represents the unit number
	// manggagamit represents the user who owns the unit

	switch (unit) {
		// declare multiple cases to represent each outcome that will match the value insidde the expression.
		case '1': {
			manggagamit = "John Travolta";
			alert('Computer owner\'s name: ' + manggagamit);
			break;
		}
		case '2': {
			manggagamit = "Steve Jobs";
			alert('Computer owner\'s name: ' + manggagamit);
			break;
		}
		case '3': {
			manggagamit = "Sid Meier"
			alert('Computer owner\'s name: ' + manggagamit);
			break;
		}
		case '4': {
			manggagamit = "Onel"
			alert('Computer owner\'s name: ' + manggagamit);
			break;
		}
		case '5': {
			manggagamit = "Christian"
			alert('Computer owner\'s name: ' + manggagamit);
			break;
		}
		default: {
			manggagamit = "mali yan kapatid, walang ganyan";
			alert(manggagamit);
		}
			// if al else fails or if none of the preceeding cases above meets the expression, this statement will become the fail safe/defualt response.
	}
	console.log(manggagamit);
}




// let dialog = "'Drink your water bhie' - mimiyuhh says";
// let dialog2 =  '"I shall return" sabi ni McArthur';
// let ownership = "Aling Nena's Tindahan"
// console.log(ownership);