console.log('Hello i\'m under the water')

// TASK 1
function arithmeticTask(){
	let numA = parseInt(prompt('Provide a number:'));
	let numB = parseInt(prompt('Provide another number:'));

	let sum = numA + numB;
	let difference = numA - numB;
	let product = numA * numB;
	let quotient  = numA / numB;

	if (sum < 10){
		console.log('The sum is: ' + sum);
	} else if (sum <= 20){
		alert('The difference is: ' + difference);
	} else if (sum <= 29){
		alert('The product is: ' + product);
	} else if (sum >= 30)
		alert('The quotient is: ' + quotient);
	}


// TASK 2
function nameTask(){
	let name = (prompt('What is your name?'));
	let age = parseInt(prompt('What is your age?'));

	if (name === "" || name === 'null') {
		alert("Are you a time traveler?");
} else if (name !== "" && age !== "") {
		alert("Hello " + name + ". Your age is " + age + ".");
}
}



// TASK 3
function getAgeStatus(){
	let age = prompt('Provide an age:');
    switch(age) {
        case '18': {
            alert("You are now allowed to party.");
            break;
        }
        case '21': {
            alert("You are now part of the adult society.");
            break;
        }
        case '65': {
            alert("We thank you for your contribution to society.");
            break;
        }
        default: {
            alert("Are you sure you're not an alien?");
        }
    }       
}


















// DISCUSSION FILE
function ageChecker(){
	// try-catch statement
	// get input
	// document parameter describes the HTML docs linked
	let userInput = document.getElementById('age').value;
	// we will now target the element where we will display the output of this function
	let message = document.getElementById('outputDisplay');
	try {

		if (userInput === '') throw 'the input is empty';

		if (isNaN(userInput)) throw 'the input is Not a Number';
		if (userInput <= 0) throw 'Not Valid Input'
		if (userInput <= 7) throw 'Good for Preschool'
		if (userInput > 7) throw 'Old for Preschool'
	} catch(err) {
		message.innerHTML = "Age Input: " + err;
	} finally {
		alert('This is from the finally section');	
	}
}